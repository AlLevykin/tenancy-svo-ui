import { useSelector } from 'react-redux';
import { Offcanvas, Form, Button, Alert } from 'react-bootstrap';
import store from '../../store';

function SignForm() {

    const show = useSelector(state => state.applications.signForm.show);
    const application = useSelector(state => state.applications.signForm.application);

    const handleClose = () => store.dispatch.applications.hideSignForm();

    const handleSubmit = (event) => {
        event.preventDefault();
        store.dispatch.applications.sendApplication(application);
        store.dispatch.contracts.addItem(application);        
        store.dispatch.applications.hideSignForm();
    }

    return (
        <Offcanvas show={show} onHide={handleClose} placement="end">
            {
                application &&
                <>
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title className="display-6">Подписание контракта</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                        <Form  onSubmit={handleSubmit}>
                            <Alert variant="warning">
                                <Alert.Heading>Арендатор</Alert.Heading>
                                <p>
                                    {
                                        `${application.ContrName} ,
                                        ИНН: ${application.Inn},
                                        КПП: ${application.Kpp},
                                        Банк: ${application.BankBill},
                                        БИК: ${application.Bik},
                                        Счет: ${application.Bill}`
                                    }
                                </p>
                            </Alert>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Предмет аренды
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={application.ContractText} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Общая площадь, кв. м
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={application.S} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Арендная плата (МГП) за кв. м в мес., без НДС и эксплуатационных расходов
                                </Form.Label>
                                <Form.Control name="price" type="number" size="lg" defaultValue={application.price} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Концессия, %
                                </Form.Label>
                                <Form.Control name="Mgt" type="number" size="lg" defaultValue={application.Mgt} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Срок аренды
                                </Form.Label>
                                <Form.Control name="PayTime" type="number" size="lg" defaultValue={application.PayTime} disabled readOnly />
                            </Form.Group>
                            <Button type="submit" variant="success" className="w-100" size="lg">Подписать</Button>
                        </Form>
                    </Offcanvas.Body>
                </>
            }
        </Offcanvas>
    );
}

export default SignForm;