import { useEffect } from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Offcanvas, Form, Button } from 'react-bootstrap';
import { nanoid } from 'nanoid'
import store from '../../store';

function ApplicationForm() {

    const show = useSelector(state => state.stock.applicationForm.show);
    const lot = useSelector(state => state.stock.applicationForm.lot);

    const [contract, setContract] = useState({
        price: 0,
        Mgt: 0,
        PayTime: 0
    });

    useEffect(() => {
        if (lot !== undefined) {
            setContract(prevState => ({
                ...prevState,
                price: lot.price
            }))
        }
    }, [lot]);

    const handleClose = () => store.dispatch.stock.hideApplicationForm();

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setContract(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        if (contract.Mgt > 0 && contract.PayTime > 0 && contract.price > 0) {

            store.dispatch.applications.addApplication({
                gid: nanoid(),
                Type: "generic",
                ID: "",
                ContrName: "ООО Ромашка",
                Inn: "7450046223",
                Kpp: "746001001",
                Bill: "40702810300000038934",
                BankBill: "ВТБ24 (ПАО) г. Москва",
                Bik: "044525716",
                ContractText: lot.name,
                Accepted: true,
                Mgt: contract.Mgt,
                PayTime: contract.PayTime,
                Peny: 0,
                value: lot.S * contract.price,
                S: lot.S,
                price: contract.price,
                paymentStatus: lot.S * contract.price > 200 ? "success" : "warning",
                payment: lot.S * contract.price > 200 ? "Одобрен" : "На рассмотрении",
                lot: lot.id
            });
            store.dispatch.stock.hideApplicationForm();
        }
    }

    return (
        <Offcanvas show={show} onHide={handleClose} placement="end">
            {
                lot &&
                <>
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title className="display-6">Коммерческое предложение</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Предмет аренды
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.name} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Вид деятельности
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.type} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Общая площадь, кв. м
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.S} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Арендная плата (МГП) за кв. м в мес., без НДС и эксплуатационных расходов
                                </Form.Label>
                                <Form.Control name="price" type="number" size="lg" value={contract.price} required onChange={handleChange} />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Концессия, %
                                </Form.Label>
                                <Form.Control name="Mgt" type="number" size="lg" required onChange={handleChange} />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Срок аренды
                                </Form.Label>
                                <Form.Control name="PayTime" type="number" size="lg" required onChange={handleChange} />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Концепция
                                </Form.Label>
                                <Form.Control as="textarea"
                                    style={{ height: '100px' }} />
                            </Form.Group>
                            <Button variant="warning" type="submit" className="w-100" size="lg">
                                Направить коммерческое предложение
                            </Button>
                        </Form>
                    </Offcanvas.Body>
                </>
            }
        </Offcanvas>
    );
}

export default ApplicationForm;