import { Badge, Carousel, Button, Row } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import store from '../../store';
import ApplicationForm from './ApplicationForm';

function Lot() {

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const id = searchParams.get('id');
    const lot = useSelector(state => state.stock.items.find(item => item.id === id));

    const handleShowApplicatioForm = (id) => {
        store.dispatch.stock.showApplicationForm(lot);
    }

    return (
        <div className="p-3">
            {
                lot &&
                <>
                    <Row className="my-3">
                        <h1 className="display-6 text-secondary">{lot.name}</h1>
                        <hr />
                        <h2 className="text-secondary">Площадь: <Badge bg="warning" text="dark"> {lot.S} кв. м. </Badge></h2>
                        <h2 className="text-secondary mb-3">Минимальные коммерческие условия (МГП за кв. м в мес., без НДС) <Badge bg="warning" text="dark"> от {lot.price} у.е. </Badge></h2>
                        <Carousel variant="dark">
                            {
                                lot.pics.map(pic =>
                                    <Carousel.Item className="text-center">
                                        <img
                                            className="d-block m-auto"
                                            src={`api/${pic}`}
                                            alt={pic}
                                        />
                                    </Carousel.Item>
                                )}
                        </Carousel>
                    </Row>
                    <Row className="my-3">
                        <Button variant="warning" size="lg" onClick={() => handleShowApplicatioForm(lot.id)}>
                            Направить коммерческое предложение
                        </Button>
                    </Row>
                    <ApplicationForm />
                </>
            }

        </div>
    );
}

export default Lot;