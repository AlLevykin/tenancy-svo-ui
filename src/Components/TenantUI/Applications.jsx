import { useSelector } from 'react-redux';
import { Row, Table, Button } from 'react-bootstrap';
import store from '../../store';
import SignForm from './SignForm';

function Applications() {

    const items = useSelector(state => state.applications.items);

    const handleSign = (id) => {
        store.dispatch.applications.showSignForm(items.find(item => item.id === id));
    }

    return (
        <>
            <Row>
                <h1 className="display-6 text-secondary mb-3">Мои коммерческие предложения</h1>
            </Row>
            <Row>
                <div className="table-responsive">
                    <Table hover>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Статус</th>
                                <th>Предмет аренды</th>
                                <th>Площадь</th>
                                <th>Срок аренды</th>
                                <th>Сумма аренды</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                items.map(item =>
                                    <tr key={item.gid}>
                                        <td>
                                            {
                                                item.paymentStatus === "success" &&
                                                <Button
                                                    variant="outline-success"
                                                    onClick={() => handleSign(item.id)}
                                                >
                                                    Подписать
                                                </Button>
                                            }
                                        </td>
                                        <td className={`table-${item.paymentStatus}`}>{item.payment}</td>
                                        <td>{item.ContractText}</td>
                                        <td>{item.S}</td>
                                        <td>{item.PayTime}</td>
                                        <td>{item.value}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </Table>
                </div>
            </Row>
            <SignForm />
        </>
    );
}

export default Applications;