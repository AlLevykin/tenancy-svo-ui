import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Table, Button } from 'react-bootstrap';
import store from '../../store';
import ContactForm from './ContractForm';

function Contracts() {

    const items = useSelector(state => state.contracts.items);

    useEffect(() => {
        store.dispatch.contracts.getContracts();
    }, []);

    const handleContractForm = (id) => {
        store.dispatch.contracts.showContractForm(items.find(item => item.id === id));
    }

    return (
        <>
            <h1 className="display-6 text-secondary mb-3">Мои площадки</h1>
            <div className="table-responsive">
                <Table hover>
                    <thead>
                        <tr>
                            <th>Контракт</th>
                            <th>Площадь</th>
                            <th>Статус контракта</th>
                            <th>Статус платежей</th>
                            <th>Менеджер</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            items.map(item =>
                                <tr key={item.id}>
                                    <td>
                                        <Button
                                            variant="outline-warning"
                                            onClick={()=> handleContractForm(item.id)}
                                        >
                                            {`${item.id} от ${item.date}`}
                                        </Button>
                                    </td>
                                    <td>{item.S}</td>
                                    <td>{item.status}</td>
                                    <td className={`table-${item.paymentStatus}`}>{item.payment}</td>
                                    <td>{item.manager}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
            </div>
            <ContactForm />
        </>
    );
}

export default Contracts;