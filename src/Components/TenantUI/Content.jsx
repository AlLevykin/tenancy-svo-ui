import { Route, Switch } from 'react-router-dom';
import Stock from './Stock';
import Lot from './Lot';
import NoMatch from './NoMatch';
import Contracts from './Contracts';
import Home from './Home';
import Applications from './Applications';

function Content() {
    return (
        <div className="p-3">
            <Switch>
                <Route path={["/", "/home.html"]} component={Home} exact />
                <Route path="/stock.html" component={Stock} exact />
                <Route path="/lot.html" component={Lot} exact />  
                <Route path="/contracts.html" component={Contracts} exact />  
                <Route path="/applications.html" component={Applications} exact />                  
                <Route component={NoMatch} />              
            </Switch>
        </div>
    );
}

export default Content;