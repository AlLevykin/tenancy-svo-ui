import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/moonrisekingdom";
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import store from '../../store';

const Donat = () => {

    const items = useSelector(state => state.contracts.items);

    useEffect(() => {
        store.dispatch.contracts.getContracts();
    }, []);

    useEffect(() => {

        if (items.length > 0) {
            am4core.useTheme(am4themes_moonrisekingdom);
            am4core.useTheme(am4themes_animated);

            let chart = am4core.create("chartdiv", am4charts.PieChart);

            chart.data = items;

            chart.innerRadius = am4core.percent(50);

            let pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "S";
            pieSeries.dataFields.category = "id";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;

            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;
        }

    }, [items]);

    return (
        <>
            <h3 className="text-warning my-3">
                Распределение площадей по контрактам
            </h3>
            <div id="chartdiv" className="w-100" style={{ height: "500px" }} >
            </div>
        </>
    )

}

export default Donat;