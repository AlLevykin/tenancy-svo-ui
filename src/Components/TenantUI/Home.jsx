import { useSelector } from 'react-redux';
import { Col, Row, Card, ListGroup, Badge } from "react-bootstrap";
import Donat from './Donat';

function Home() {
    
    const items = useSelector(state => state.contracts.items);
    const S = items.reduce((sum, current) => {
        return sum + current.S;
      }, 0);

    return (
        <>
            <Row>
                <h1 className="display-6 text-secondary mb-3">Главная</h1>
            </Row>
            <Row>
                <Col>
                    <Row>
                        <Col>
                            <Card
                                bg="secondary"
                                text="white"
                                className="mb-2 h-100"
                            >
                                <Card.Header>Площадок арендовано</Card.Header>
                                <Card.Body>
                                    <Card.Title className="display-1 text-center">{items.length}</Card.Title>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card
                                bg="warning"
                                text="dark"
                                className="mb-2 h-100"
                            >
                                <Card.Header>Общая площадь</Card.Header>
                                <Card.Body>
                                    <Card.Title className="display-1 text-center">{S}</Card.Title>
                                    <Card.Text className="text-end">
                                        кв. м
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card
                                bg="light"
                                text="dark"
                                className="mb-2 h-100"
                            >
                                <Card.Header>Счетов к оплате</Card.Header>
                                <Card.Body>
                                    <Card.Title className="display-1 text-center">0</Card.Title>
                                    <Card.Text>
                                        Благодарим за своевременную оплату счетов
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                    <Donat />
                    </Row>
                </Col>
                <Col>
                    <h3 className="text-warning mb-3">
                        Новости и уведомления
                    </h3>
                    <ListGroup variant="flush">
                        <ListGroup.Item>
                            Вам необходимо внести платёж по договору №23132 до 31.11.2021, пожалуйста, сделайте это вовремя.
                            <br />
                            <Badge bg="light" text="dark">Команда “Шереметьево”, 20.11.2021</Badge>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Спасибо за своевременный платёж, мы рады, что вы с нами.
                            <br />
                            <Badge bg="light" text="dark">Команда “Шереметьево”, 15.10.2021</Badge>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Вам пришел счет по контракту №213245, перейдите в раздел “Мои счета”, чтобы ознакомиться и оплатить.
                            <br />
                            <Badge bg="light" text="dark">Команда “Шереметьево”, 03.09.2021</Badge>
                        </ListGroup.Item>
                    </ListGroup>
                    <h3 className="text-warning my-3">
                        Стандарты и правила
                    </h3>
                    <ListGroup variant="flush">
                        <ListGroup.Item>Памятка для новых арендаторов</ListGroup.Item>
                        <ListGroup.Item>Инструкция о мерах пожарной безопасности</ListGroup.Item>
                        <ListGroup.Item>Инструкция по приему-передаче помещений</ListGroup.Item>
                        <ListGroup.Item>Правила пользования АВК</ListGroup.Item>
                        <ListGroup.Item>СТП О мерах пожарной безопасности</ListGroup.Item>
                        <ListGroup.Item>Требования к арендаторам</ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </>
    );
}

export default Home;