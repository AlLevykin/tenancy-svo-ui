import { useSelector } from 'react-redux';
import { Offcanvas, Form, Button } from 'react-bootstrap';
import store from '../../store';

function ContactForm() {

    const show = useSelector(state => state.contracts.contractForm.show);
    const contract = useSelector(state => state.contracts.contractForm.contract);
    const lot = useSelector(state => {
        return (
            contract ?
                state.stock.items.find(item => item.id === contract.lot)
                :
                null
        )
    });

    const handleClose = () => store.dispatch.contracts.hideContractForm();

    return (
        <Offcanvas show={show} onHide={handleClose} placement="end">
            {
                contract &&
                <>
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title className="display-6">Контракт</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Offcanvas.Body>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Номер
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={`${contract.id} от ${contract.date}`} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Статус
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={contract.status} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Статус платежей
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={contract.payment} disabled readOnly />
                            </Form.Group>
                            <Button variant={contract.paymentStatus} className="w-100" size="lg">Оплатить</Button>
                        </Form>
                        <hr />
                        <Offcanvas.Title className="display-6">Предмет аренды</Offcanvas.Title>
                        <Form>
                            <Form.Group className="mb-3">
                                <Form.Control type="text" size="lg" defaultValue={lot.name} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Вид деятельности
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.type} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Общая площадь, кв. м
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.S} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Арендная плата (МГП) за кв. м в мес., без НДС и эксплуатационных расходов
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue={lot.price} disabled readOnly />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Концессия, %
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue="10" disabled readOnly  />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>
                                    Срок аренды
                                </Form.Label>
                                <Form.Control type="text" size="lg" defaultValue="3 года" disabled readOnly  />
                            </Form.Group>
                        </Form>
                    </Offcanvas.Body>
                </>
            }
        </Offcanvas>
    );
}

export default ContactForm;