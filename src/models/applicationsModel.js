//import { sendData } from './utils';

export const applicationsModel = {
    name: 'applications',
    state: {
        items: [],
        signForm: {
            show: false
        }
    },
    reducers: {
        addApplication(state, application) {
            return {...state, items: [...state.items, application]};
        },
        sendApplication(state, application) {
            return {...state, items: state.items.filter(item => item.gid !== application.gid)};
        },
        hideSignForm(state) {
            return {...state, signForm: {show: false}};
        },
        showSignForm(state, application) {
            return {...state, signForm: {show: true, application: application}};
        }              
    },
/*     effects: {
        async sendApplication() {
            await sendData('api/contracts.json').then(items =>
                this.updateItems(items)
            );
        }
    } */
}