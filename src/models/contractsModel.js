import { getData, randomInteger, formatDate } from './utils';

export const contractsModel = {
    name: 'contracts',
    state: {
        items: [],
        contractForm: {
            show: false
        }
    },
    reducers: {
        updateItems(state, items) {
            if (state.items.length > 0) {
                return state
            } else {
                return { ...state, items: items };
            }
        },
        addItem(state, application) {
            const date = new Date();
            const contract = {
                id: `А-${randomInteger(1, 100)}`,
                date: formatDate(date),
                S: application.S,
                status: "Действующий",
                payment: "Требует оплаты",
                paymentStatus: "warning",
                manager: "Смирнов С.С.",
                lot: application.lot
            };
            return { ...state, items: [...state.items, contract] };
        },
        hideContractForm(state) {
            return { ...state, contractForm: { show: false } };
        },
        showContractForm(state, contract) {
            return { ...state, contractForm: { show: true, contract: contract } };
        }
    },
    effects: {
        async getContracts() {
            await getData('api/contracts.json').then(items =>
                this.updateItems(items)
            );
        }
    }
}