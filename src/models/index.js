import { stockModel } from './stockModel';
import { contractsModel } from './contractsModel';
import { applicationsModel } from './applicationsModel';

const models = {
    stockModel,
    contractsModel,
    applicationsModel
};

export default models;